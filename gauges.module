<?php

/**
 * @file
 * Drupal Module: Gauges
 * Adds required Javascrpt to allow tracking with GAuges
 *
 * @author: Grant Lucas <http://drupal.org/user/501320>
 */

// Remove trackig from admin pages by default.
define('GAUGES_PAGES', "admin\nadmin/*\nbatch\nnode/add*\nnode/*/*\nuser/*/*");

/**
 * Implmentation of hook_menu().
 */
function gauges_menu() {
  $items = array();

  $items['admin/config/system/gauges'] = array(
    'title'            => 'Gauges',
    'description'      => 'Configure the tracking settings for Gauges.',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('gauges_settings_form'),
    'access arguments' => array('administer gauges'),
  );

  return $items;
}

/**
 * Implmentation of hook_permission().
 */
function gauges_permission() {
  return array(
    'administer gauges' => array(
      'title' => t("Administer Gauges"),
      'description' => t("Configure settings for Gauges integration."),
    ),
  );
}

/**
 * Gauges Settings form.
 */
function gauges_settings_form($form, &$form_state) {

  // Gauge note on Gauges' admin browser tracking.
  $form['gauge_note'] = array(
    '#type'   => 'item',
    '#title'  => t("Note about Gauges and your browser"),
    '#markup' => t("In order to reduce the possibility of tracking yourself, Gauges will not track any browsers you have used to log into Gauges with. Because of this, when testing that this module is working, ensure that you are using a browser you haven't used to log into Gauges with."),
  );

  // Gauge ID.
  $form['gauges_gauge_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t("Gauge ID"),
    '#description'   => t('This is the unique ID provided by Gauges. It can be found under "Tracking Code" for the Gague you want to link this site to.'),
    '#required'      => TRUE,
    '#default_value' => variable_get('gauges_gauge_id', ''),
  );

  // Tracking Scope.
  $form['tracking_scope_title'] = array(
    '#type'  => 'item',
    '#title' => t("Tracking Scope"),
  );

  // TODO: Add JS for setting the summary in tracking vertical tabs.
  $form['tracking_scope'] = array(
    '#type' => 'vertical_tabs',
  );

  // Pages.
  $form['tracking_scope']['pages'] = array(
    '#type'  => 'fieldset',
    '#title' => t("Pages"),
  );

  $options = array(
    t('Every page excpet the listed pages'),
    t('The listed pages only'),
  );

  $form['tracking_scope']['pages']['gauges_visibility_pages'] = array(
    '#type'          => 'radios',
    '#title'         => t("Add tracking to specific pages"),
    '#options'       => $options,
    '#default_value' => variable_get('gauges_visibility_pages', 0),
  );

  $form['tracking_scope']['pages']['gauges_pages'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Pages'),
    '#title_display' => 'invisible',
    '#description'   => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page."),
    '#default_value' => variable_get('gauges_pages', GAUGES_PAGES),
    '#rows'          => 10,
  );

  return system_settings_form($form);
}

/**
 * Settings validation function.
 */
function gauges_settings_form_validate($form, &$form_state) {
  // Trim text values before storage.
  $form_state['values']['gauges_gauge_id']     = trim($form_state['values']['gauges_gauge_id']);
  $form_state['values']['gauges_pages'] = trim($form_state['values']['gauges_pages']);
}

/**
 * Implmentation of hook_page_alter().
 */
function gauges_page_alter(&$page) {
  $gauges_gauge_id = variable_get('gauges_gauge_id', '');

  // Only proceed if the gauge ID was set and visibility is true for this page.
  if (!empty($gauges_gauge_id) && _gauges_visibility_pages()) {
    // Build the tracking script.
    $script = "var _gauges = _gauges || [];";
    $script .= "(function() {";
    $script .= "var t   = document.createElement('script');";
    $script .= "t.type  = 'text/javascript';";
    $script .= "t.async = true;";
    $script .= "t.id    = 'gauges-tracker';";
    $script .= "t.setAttribute('data-site-id', '" . $gauges_gauge_id . "');";
    $script .= "t.src = '//secure.gaug.es/track.js';";
    $script .= "var s = document.getElementsByTagName('script')[0];";
    $script .= "s.parentNode.insertBefore(t, s);";
    $script .= "})();";

    // As per Gauges documentation, add it to the footer scope.
    drupal_add_js($script, array('scope' => 'footer', 'type' => 'inline'));
  }
}

/**
 * Check if the code should be included on the current page.
 */
function _gauges_visibility_pages() {
  static $page_match;

  // Cache visibility if this function is called more than once.
  if (!isset($page_match)) {
    $visibility = variable_get('gauges_visibility_pages', 0);
    $pages      = variable_get('gauges_pages', GAUGES_PAGES);

    // Match path.
    if (!empty($pages)) {
      $pages = drupal_strtolower($pages);
      if ($visibility < 2) {
        // Convert the Drupal path to lowercase.
        $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));

        // Compare the lowercase internal and lowercase path alias (if any).
        $page_match = drupal_match_path($path, $pages);

        if ($path != $_GET['q']) {
          $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
        }

        // When $visibility has a value of 0, the tracking code is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $page_match = !($visibility xor $page_match);
      }
      else {
        $page_match = FALSE;
      }
    }
    else {
      $page_match = TRUE;
    }
  }

  return $page_match;
}
