Module: Gauges
Author: [Grant Lucas](http://drupal.org/user/501320)

## Description

Adds support for [Gauges](http://get.gaug.es/) web analytics tracking.

This module requires a Gauges account which can be registered at [Gauges](http://get.gaug.es/)

## Current Features

- Selectively track/exclude certain pages

## Planned Features
- Ability to track/exclude certain users and roles
